# to-do-app
Sarah Büttler
Nicole Bütikofer

Minimum requirements:
	• Enter and save a task
	• One task at a time is shown. You can skip through the list. If the end is reached, the first task is shown again, same in the other direction
	• Set task as done
	• Edit task
	• Delete task
Additional features
	• A list which shows the 5 most recent done tasks -> list can be deleted
	• A counter shows how many tasks are open at the moment
	• A motivating picture
	• Css styling
	• Stage icon
	• Save button is only activated if a task is valid
	• Task is valid if > 2 characters
	• If task is invalid, text color is red
	• If task is valid, text coor and border of textfield are green
	• Done, Edit, Delete and buttons to skip are only activated if at least one task is open
	• Button to delete the "Done list" is only avtivated when at least one task is shown as done
	• Buttons with icons
	• When the program closes,  all data is saved to a file 
    • When the program starts, it check for a file, and loads data automatically 
