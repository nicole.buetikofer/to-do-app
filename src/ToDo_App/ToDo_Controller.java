package ToDo_App;

import javafx.beans.binding.Bindings;
import javafx.scene.input.MouseEvent;

public class ToDo_Controller {
	private ToDo_Model model;
	private ToDo_View view;
	
	private boolean taskValid;
	private int counter = 0;
	
	public ToDo_Controller(ToDo_View view, ToDo_Model model) {
		this.model = model;
		this.view = view;
		
		view.saveButton.setOnMouseClicked(this::saveNewTask);
		view.doneButton.setOnMouseClicked(this::taskDone);
		view.editButton.setOnMouseClicked(this::editTask);
		view.deleteButton.setOnMouseClicked(this::deleteTask);
		view.nextButton.setOnMouseClicked(this::showNextTask);
		view.backButton.setOnMouseClicked(this::showLastTask);	
		view.deleteDoneListButton.setOnMouseClicked(this::deleteDoneList);
		
		view.enterTaskTF.textProperty().addListener(
				(observable, oldValue, newValue) -> validateTask(newValue));
			
		view.deleteButton.disableProperty().bind(Bindings.isEmpty(view.showTask.textProperty()));
		view.doneButton.disableProperty().bind(Bindings.isEmpty(view.showTask.textProperty()));
		view.editButton.disableProperty().bind(Bindings.isEmpty(view.showTask.textProperty()));
		view.nextButton.disableProperty().bind(Bindings.isEmpty(view.showTask.textProperty()));
		view.backButton.disableProperty().bind(Bindings.isEmpty(view.showTask.textProperty()));
		view.deleteDoneListButton.disableProperty().bind(Bindings.isEmpty(view.done1.textProperty()));
	}
	//Task is only valid if length > 2. If invalid, text is red
	private void validateTask(String newValue) {
		boolean valid = false;
		String s = view.enterTaskTF.getText();
		if (s.length() > 2) {
			valid = true;
		}
		
		view.enterTaskTF.getStyleClass().remove("taskOK");
		view.enterTaskTF.getStyleClass().remove("taskNotOK");
		if (valid) {
			view.enterTaskTF.getStyleClass().add("taskOK");
		} else {
			view.enterTaskTF.getStyleClass().add("taskNotOK");
	}
		//Save result
		taskValid = valid;
		//Enable button if valid
		enableSaveButton();
	}

	private void enableSaveButton() {
		boolean valid = taskValid;
		view.saveButton.setDisable(!valid);
		
	}

	private void saveNewTask(MouseEvent e) {
		//get Text from TF
		String task= new String();
		task = view.enterTaskTF.getText();
		//parse String into todo object
		ToDo t = new ToDo(task);
		//add task to ArrayList in separate method
		model.addTask(t);
		
		//empty the TextField
		view.enterTaskTF.clear();
		
		int y = model.tasks.size() -1;
		ToDo todo = model.tasks.get(y);
		String s = todo.getTask();
		view.showTask.setText(s);
		
		updateCounter();
	}
	
	private void updateCounter() {
		int i = model.tasks.size();
		String s = String.valueOf(i);
		view.countTasks.setText(s);
		
	}
	private void taskDone(MouseEvent e) {
		//Delete task from ArrayList and add to the section Done
		ToDo t = getTaskFromTF();

			
		//Add task to ArrayList doneTasks
		model.addToDoneArray(t);
		
		//Show task in Done-field
		addToGrid(t);
		//Remove from TextField
		removeTaskFromList(t);
		
		//Remove from arraylist
		model.removeTaskFromArray(t);	
		
		updateCounter();
	}

	
	
	private void addToGrid(ToDo t) {
		int i = counter % 5;
		String s = t.getTask();

		switch (i) {
		case 0: view.done1.setText(s); break;
		case 1: view.done2.setText(s); break;
		case 2: view.done3.setText(s); break;		
		case 3: view.done4.setText(s); break;
		case 4: view.done5.setText(s); break;
		
		}counter ++;
	
	}

	private void deleteTask(MouseEvent e) {			
		// Get task from TextField
		ToDo t = getTaskFromTF();
				
		//Remove from TextField
		removeTaskFromList(t);
				
		//Remove from arraylist
		model.removeTaskFromArray(t);
		
		updateCounter();
	
	}
	
	private void editTask(MouseEvent e) {
		
		ToDo t = getTaskFromTF();
		String s = t.getTask();
		

		if(s != "") {
		// Add text to TF
		view.enterTaskTF.setText(s);
		
		removeTaskFromList(t);
		model.removeTaskFromArray(t);
		
		updateCounter();
	}
	}
	
	private ToDo getTaskFromTF() {
		String s = view.showTask.getText();
		ToDo t = new ToDo(s);
		
		return t;
	}
	
	private void removeTaskFromList(ToDo t) {
	
		int i = model.getPosInArray(t);
		
		if (i == 0) {
			if (model.tasks.size() == 1) {
				view.showTask.setText("");
		} else {
			ToDo task = model.tasks.get(i+1);
			String s = task.getTask();
			view.showTask.setText(s);
		}		
		}else {
			ToDo task = model.tasks.get(i-1);
			String s = task.getTask();
			view.showTask.setText(s);
		}
		}


	private void showNextTask(MouseEvent e) {
	
		ToDo t = getTaskFromTF();
		int pos = model.getPosInArray(t);
		
		//check if it is the last Task. If yes, show first task
		int size = model.tasks.size();
		if (size > 1) {
		if (pos+1 < size) {
			// set Label showTask to the next String
			ToDo t2 = model.tasks.get(pos+1);
			String s2 = t2.getTask();
			view.showTask.setText(s2);
		} else {
			// set Label showTask to first Task in the list
			ToDo t2 = model.tasks.get(0);
			String s2 = t2.getTask();
			view.showTask.setText(s2);
		}
		}
	}

	private void showLastTask(MouseEvent e) {
		ToDo t = getTaskFromTF();
		int pos = model.getPosInArray(t);
		
		int size = model.tasks.size();
		if (size > 1) {
		//check if it is the first task. If yes, show last task
		if (pos != 0) {
			//set Label showTask to the String before this one
			ToDo t2 = model.tasks.get(pos-1);
			String s2 = t2.getTask();
			view.showTask.setText(s2);
		}else {
			//set Label showTask to last Task in the list
			ToDo t2 = model.tasks.get(size-1);
			String s2 = t2.getTask();
			view.showTask.setText(s2);
		}
		}
	}

	private void deleteDoneList(MouseEvent mouseevent1) {
		view.done1.setText("");
		view.done2.setText("");
		view.done3.setText("");
		view.done4.setText("");
		view.done5.setText("");
		
		model.emptyDoneArray();
		this.counter= 0;
	}
	
	public void updateView() {
		model.loadTasks();
		model.loadDoneTasks();
		showTask();
		showDoneTasks();
	
}
	private void showDoneTasks() {
		if(model.doneTasks.size()>0) {
			 int i = model.doneTasks.size();
			if (model.doneTasks.size()>=5) {
				//show the last 5 tasks in doneArray in label done1 - done5
					ToDo t5 = model.doneTasks.get(i-1);
					String s5= t5.getTask();
					view.done5.setText(s5);
			
					ToDo t4 = model.doneTasks.get(i-2);
					String s4 = t4.getTask();
					view.done4.setText(s4);
		
					ToDo t3 = model.doneTasks.get(i-3);
					String s3 = t3.getTask();
					view.done3.setText(s3);
			
					ToDo t2 = model.doneTasks.get(i-4);
					String s2 = t2.getTask();
					view.done2.setText(s2);
			
					ToDo t1 = model.doneTasks.get(i-5);
					String s1 = t1.getTask();
					view.done1.setText(s1);
				}else {
					//if less than 5 show all tasks in doneTasks
					switch (i) {
					case 5: 
						ToDo t5 = model.doneTasks.get(4);
						String s5= t5.getTask();
						view.done5.setText(s5);
					case 4: 
						ToDo t4 = model.doneTasks.get(3);
						String s4= t4.getTask();
						view.done4.setText(s4);
					case 3: 
						ToDo t3 = model.doneTasks.get(2);
						String s3= t3.getTask();
						view.done3.setText(s3);
					case 2: 
						ToDo t2 = model.doneTasks.get(1);
						String s2= t2.getTask();
						view.done2.setText(s2);
					case 1: 
						ToDo t1 = model.doneTasks.get(0);
						String s1= t1.getTask();
						view.done1.setText(s1); break;
				}
					
			}
	
	}
	}
	
	
	private void showTask() {
		if(model.tasks.size()>0) {
			updateCounter();
			ToDo t = model.tasks.get(0);
			String s = t.getTask();
			view.showTask.setText(s);
			}
		
	}
}
