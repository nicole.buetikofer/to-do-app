package ToDo_App;

public class ToDo {
private String task;

	
	public ToDo (String task) {
		this.task = task;
	}
	
	public String getTask() {
		return task;
	}
	
	public void setTask(String task) {
		this.task = task;
	}
	
	public boolean equals(ToDo t) {
		return (this.task == t.task);
	}
		
}


