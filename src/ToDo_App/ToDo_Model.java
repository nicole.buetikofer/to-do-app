package ToDo_App;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;


public class ToDo_Model {
	protected ArrayList<ToDo> tasks = new ArrayList<ToDo>();
	protected ArrayList<ToDo> doneTasks = new ArrayList<ToDo>();
	
	private static String TASKS_FILE = "tasks.txt";
	private static String DONETASKS_FILE = "donetasks.txt";
	private static String SEPARATOR = ";"; // Separator for "split"
		
	public void addTask(ToDo t) {
		//Task should be added to first empty place in the Array list
		int i = tasks.size();		
		tasks.add(i, t);
				
	}
	
	public void removeTaskFromArray(ToDo t) {
		// Remove the task from the ArrayList tasks
		int i = 0;
		 for (ToDo toDo : tasks) {
			 if (t.equals(toDo)) {
				i = tasks.indexOf(toDo); 
				
			 }
			}tasks.remove(i);	
	}

	public int getPosInArray(ToDo t) {
	int i = 0;
		
		for (ToDo toDo : tasks) {
			
			if(t.equals(toDo)) {
				i = tasks.indexOf(toDo);	
			}
		}
		return i;
		
	}
	
	public void addToDoneArray(ToDo t) {
		doneTasks.add(t);
		
	}

	public int getFirstEmptyPosInDoneArray() {
		//Get the first empty position in doneArray
		return doneTasks.size();
	}

	public void emptyDoneArray() {
		doneTasks.clear();
		
	}
		
	public void loadTasks() {
		File tasksFile = new File(TASKS_FILE);
		try (Reader inReader = new FileReader(tasksFile)) {
			BufferedReader in = new BufferedReader(inReader);
			
			String line = in.readLine();
			while (line != null) {
				ToDo t = readTask(line);
				tasks.add(t);
				line = in.readLine();
			}
		} catch (Exception e) {
			System.out.println("tasks.txt not existing");
		}
		
	}

	private ToDo readTask(String line) {
		String[] attributes = line.split(SEPARATOR);
		String task = attributes[0];
		ToDo t = new ToDo(task);
		return t;
	}
	
	public void saveTasks() {
		File tasksFile = new File(TASKS_FILE);
		try (Writer out = new FileWriter(tasksFile)) {
			for (ToDo t : tasks) {
				String line = writeTask(t);
				out.write(line);
			}
		} catch (Exception e) {
			//Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String writeTask(ToDo t) {
		String line = t.getTask() + SEPARATOR + "\n";
		return line;
	}

	public void loadDoneTasks() {
		File doneTasksFile = new File(DONETASKS_FILE);
		try (Reader inReader = new FileReader(doneTasksFile)) {
			BufferedReader in = new BufferedReader(inReader);
			
			String line = in.readLine();
			while (line != null) {
				ToDo t = readTask(line);
				doneTasks.add(t);
				line = in.readLine();
			}
		} catch (Exception e) {
			System.out.println("donetasks.txt not existing");
		}
		
	}
	public void saveDoneTasks() {
		File doneTasksFile = new File(DONETASKS_FILE);
		try (Writer out = new FileWriter(doneTasksFile)) {
			for (ToDo t : doneTasks) {
				String line = writeTask(t);
				out.write(line);
			}
		} catch (Exception e) {
			//Auto-generated catch block
			e.printStackTrace();
		}
	}


}
