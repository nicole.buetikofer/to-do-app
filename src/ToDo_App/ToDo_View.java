package ToDo_App;


import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ToDo_View {
	private Stage stage;
	private ToDo_Model model;
	protected Button saveButton, doneButton, deleteButton, editButton, deleteDoneListButton;
	protected Button backButton, nextButton;
	protected TextField enterTaskTF;
	protected Label showTask, countTasks;
	protected Label done1, done2, done3, done4, done5;


	private final Image quote = new Image("quote.png");
	private Image iconToDo = new Image("Icon_ToDo.png");
	private Image arrowRight = new Image("arrow_right.png");
	private ImageView arrowR = new ImageView(arrowRight);
	private Image arrowLeft = new Image("arrow_left.png");
	private ImageView arrowL = new ImageView(arrowLeft);
	private Image doneImage = new Image("done.png");
	private ImageView done = new ImageView(doneImage);
	private Image editImage = new Image("edit.png");
	private ImageView edit = new ImageView(editImage);
	private Image deleteImage = new Image("delete.png");
	private ImageView delete = new ImageView(deleteImage);
	private Image saveImage = new Image("save.png");
	private ImageView save = new ImageView(saveImage);
	
	private ImageView image;
	
	public ToDo_View(Stage stage, ToDo_Model model) {
		this.stage = stage;
		this.model = model;
				
		VBox vboxLeft = new VBox();
		vboxLeft.getStyleClass().add("vboxLeft");
		VBox vboxRight = new VBox();
		vboxRight.getStyleClass().add("vboxRight");
		HBox root = new HBox();
		root.getStyleClass().add("HBox");
		
				
		//Left part of the GUI
		Label lbl = new Label ("Enter a task");
		enterTaskTF = new TextField();
		saveButton = new Button ();
		saveButton.setMinSize(50, 31);
		saveButton.setGraphic(save);
		saveButton.setDisable(true);
		
	
		this.image = new ImageView(quote);
		
		vboxLeft.getChildren().addAll(lbl, enterTaskTF, saveButton, image);
		
		//right upper Part of the GUI
		GridPane taskGP = new GridPane();
		taskGP.setHgap(5);
		taskGP.setVgap(5);
		Label taskLbl = new Label ("Tasks: ");
		this.countTasks = new Label("0");
		
		showTask = new Label();
		//Done Button
		doneButton = new Button();
		doneButton.setMinSize(50, 31);
		doneButton.setGraphic(done);
		//Delete Button
		deleteButton = new Button ();
		deleteButton.setMinSize(50, 31);
		deleteButton.setGraphic(delete);
		//Edit Button
		editButton = new Button ();
		editButton.setMinSize(50, 31);
		editButton.setGraphic(edit);
		
		backButton = new Button();
		backButton.setMinSize(50, 31);
		backButton.setGraphic(arrowL);
		nextButton = new Button();
		nextButton.setMinSize(50, 31);
		nextButton.setGraphic(arrowR);
		
		taskGP.add(taskLbl, 0, 0);
		taskGP.add(countTasks, 1, 0);
		taskGP.add(showTask, 0, 1, 4, 1);
		taskGP.add(doneButton, 0, 4);
		taskGP.add(editButton, 1, 4);
		taskGP.add(deleteButton, 2, 4);
		taskGP.add(backButton, 0, 3, 2, 1);
		taskGP.add(nextButton, 1, 3, 3, 1);
		
		//right down part of the GUI
		GridPane doneGP = new GridPane();
		doneGP.setVgap(5);
		done1 = new Label();
		done2 = new Label();
		done3 = new Label();
		done4 = new Label();
		done5 = new Label();
		deleteDoneListButton = new Button("Delete List");
		
		doneGP.add(done1, 0, 1);
		doneGP.add(done2, 0, 2);
		doneGP.add(done3, 0, 3);
		doneGP.add(done4, 0, 4);
		doneGP.add(done5, 0, 5);
		doneGP.add(deleteDoneListButton, 5, 6);
		
		Label doneLbl = new Label("Tasks completed last");
		
		root.getChildren().addAll(vboxLeft, vboxRight);
		doneGP.add(doneLbl, 0, 0);
			
		vboxRight.getChildren().addAll(taskGP, doneGP);
		
		
		Scene scene = new Scene(root, 580, 370);
		scene.getStylesheets().add(
				getClass().getResource("ToDo.css").toExternalForm());
		stage.setScene(scene);
		this.iconToDo = new Image("Icon_ToDo.png");
		stage.getIcons().add(iconToDo);
		stage.setTitle("To-Do App");
	}
	
	public void start() {
		stage.show();
		
	}
}
