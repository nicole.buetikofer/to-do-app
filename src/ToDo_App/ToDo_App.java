package ToDo_App;

import java.util.Locale;

import javafx.application.Application;
import javafx.stage.Stage;

public class ToDo_App extends Application{
	private ToDo_View view;
	private ToDo_Model model;
	private ToDo_Controller controller;

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage stage) throws Exception {
		view = new ToDo_View(stage, model);
		model = new ToDo_Model();
		controller = new ToDo_Controller(view, model);
		view.start();
		controller.updateView();
		
		Locale.setDefault(Locale.ENGLISH);
		
		
	}
	
	@Override
	public void stop() {
		model.saveTasks();
		model.saveDoneTasks();
	}
}
